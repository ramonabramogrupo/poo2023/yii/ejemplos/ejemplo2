<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'Gestion de Pedidos';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Gestion del almacen</h1>

        <p class="lead">Getion de Comerciales, Clientes y Pedidos</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="p-2 col-lg-3 m-2 border rounded">
                <h2>Clientes</h2>
                <p class="mt-2 text-center">
                    <?= Html::a('Ver Clientes', ['/cliente/index'], [
                        'class' => 'btn btn-primary'
                    ])
                    ?>
                </p>
            </div>
            <div class="p-2 col-lg-3 m-2 border rounded">
                <h2>Comerciales</h2>
                <p class="mt-2 text-center">
                    <?= Html::a('Ver Comerciales', ['/comercial/index'], [
                        'class' => 'btn btn-primary'
                    ])
                    ?>
                </p>
            </div>
            <div class="p-2 col-lg-3 m-2 border rounded">
                <h2>Pedidos</h2>
                <p class="mt-2 text-center">
                    <?= Html::a('Ver Pedidos', ['/pedido/index'], [
                        'class' => 'btn btn-primary'
                    ])
                    ?>
                </p>
            </div>
        </div>

    </div>
</div>