<?php

use app\models\Cliente;
use app\models\Comercial;
use app\models\Pedido;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Pedido $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pedido-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'total')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'id_cliente')->dropDownList(
        Pedido::todosClientes(),
        ['prompt' => 'Seleccione un cliente']
    );
    ?>

    <?= $form->field($model, 'id_comercial')->dropDownList(
        $model->todosComerciales(),
        //Comercial::todosComerciales(),
        ['prompt' => 'Seleccione un comercial']
    );
    ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>